module.exports = {
    entry: './app/entry.js',
    output: {
        path: './app/',
        filename: 'bundle.js'
    },
    devtool: "source-map",
    module: {
      loaders: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader"
        },
        {
            test: /\.scss$/,
            loaders: ['style-loader', 'css-loader', 'sass-loader']
        }
      ]
    }
};