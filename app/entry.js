import React from 'react';
import ReactDOM from 'react-dom';

import Layout from './components/Layout';

require('./scss/styles.scss');

class App extends React.Component {
	constructor() {
		super();
		this.state = {
			data: []
		};
	}
	componentWillMount() {

	}
	render() {
		return (
			<Layout />
		);
	}
}

ReactDOM.render(<App />, document.getElementById('app'));