import React from 'react';

export const Card = props => {
	const { givenName, surname, email, phone, house, street, suburb, state, postcode, country, avatar } = props;
	const avatarDefault = '../img/avatar.png';
	return (
		<div className="right">
			<div className="vcard">
				<h3>Hcard Preview</h3>
				<header>
					<h2>
						<span className="fn">
							<span className="given-name">{ givenName }</span> <span className="family-name">{ surname }</span>
						</span>
						<div className="avatar">
							<img src={ avatar || avatarDefault } className="avatar-img" alt="Avatar placeholder" />
						</div>
					</h2>
				</header>
				<div className="content">
					<div className="line">
						<label>Email</label>
						<span className="email">{ email }</span>
					</div>
					<div className="line">
						<label>Phone</label>
						<span className="tel">{ phone }</span>
					</div>
					<div className="adr">
						<div className="line">
							<label>Address</label>
							<span className="street-address">{ house } { street }</span>
						</div>
						<div className="line second">
							<label className="spacer">&nbsp;</label>
							<span className="locality">{ suburb}</span>
							<span className="region"> { state }</span>
						</div>
						<div className="row">
							<div className="col-sm-6">
								<div className="line">
									<label>Postcode</label>
									<span className="postal-code">{ postcode }</span>
								</div>
							</div>
							<div className="col-sm-6">
								<div className="line">
									<label>Country</label>
									<span className="country-name">{ country }</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}