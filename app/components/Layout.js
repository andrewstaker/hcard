import React, { Component } from 'react';

import Form from './Form';
import { Card } from './Card';

export default class Layout extends Component {
	constructor() {
		super();
		this.state = {
			givenName: '',
			surname: '',
			email: '',
			phone: '',
			house: '',
			street: '',
			suburb: '',
			state: '',
			postcode: '',
			country: '',
			avatar: ''
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleUpload = this.handleUpload.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	handleChange(e) {
		this.setState({
			[e.target.name]: e.target.value
		})
	}
	handleUpload(e) {
		const uploadUrl = window.URL.createObjectURL(e.target.files[0])
		this.setState({
			[e.target.name]: uploadUrl
		})
	}
	handleSubmit(e) {
		e.preventDefault();
	}
	render() {
		const {
			givenName,
			surname,
			email,
			phone,
			house,
			street,
			suburb,
			state,
			postcode,
			country,
			avatar
		} = this.state;
		return (
			<div className="card-wrap">
				<Form
					handleChange={this.handleChange}
					handleUpload={this.handleUpload}
					handleSubmit={this.handleSubmit}
				/>
				<Card
					givenName={givenName}
					surname={surname}
					email={email}
					phone={phone}
					house={house}
					street={street}
					suburb={suburb}
					state={state}
					postcode={postcode}
					country={country}
					avatar={avatar}
				/>
			</div>
		);
	}
}