import React, { Component } from 'react';

export default class Form extends Component {
	render() {
		const { handleChange, handleUpload, handleSubmit } = this.props;
		return (
			<div className="left">
				<h1>hCard Builder</h1>
				<form onSubmit={ handleSubmit }>
					<h4>Personal Details</h4>
					<div className="row">
						<div className="col-md-6 form-group">
							<label>Given Name</label>
							<input type="text" name="givenName" onChange={ handleChange } />
						</div>
						<div className="col-md-6 form-group">
							<label>Surname</label>
							<input type="text" name="surname" onChange={ handleChange } />
						</div>
					</div>
					<div className="row">
						<div className="col-md-6 form-group">
							<label>Email</label>
							<input type="email" name="email" onChange={ handleChange } />
						</div>
						<div className="col-md-6 form-group">
							<label>Phone</label>
							<input type="text" name="phone" onChange={ handleChange } />
						</div>
					</div>
					<h4>Address</h4>
					<div className="row">
						<div className="col-md-6 form-group">
							<label>House name or &#35;</label>
							<input type="text" name="house" onChange={ handleChange } />
						</div>
						<div className="col-md-6 form-group">
							<label>Street</label>
							<input type="text" name="street" onChange={ handleChange } />
						</div>
					</div>
					<div className="row">
						<div className="col-md-6 form-group">
							<label>Suburb</label>
							<input type="text" name="suburb" onChange={ handleChange } />
						</div>
						<div className="col-md-6 form-group">
							<label>State</label>
							<input type="text" name="state" onChange={ handleChange } />
						</div>
					</div>
					<div className="row">
						<div className="col-md-6 form-group">
							<label>Postcode</label>
							<input type="text" name="postcode" onChange={ handleChange } />
						</div>
						<div className="col-md-6 form-group">
							<label>Country</label>
							<input type="text" name="country" onChange={ handleChange } />
						</div>
					</div>
					<div className="row buttons-row">
						<div className="col-md-6">
							<input type="file" name="avatar" id="avatar" onChange={ handleUpload } />
							<label htmlFor="avatar" className="btn btn-default">Upload Avatar</label>
						</div>
						<div className="col-md-6">
							<button type="submit" className="btn btn-primary">Create hCard</button>
						</div>
					</div>
				</form>
			</div>
		);
	}
}